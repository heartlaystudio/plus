# coding:utf-8

import hashlib
import inspect
import random
import re
import time
import uuid as _uuid

from datetime import datetime
from functools import wraps, partial
from importlib import import_module
from pkgutil import iter_modules
from string import ascii_lowercase, ascii_uppercase, digits
from types import MethodType


def add_method(cls, f, name=None):
    setattr(cls, name or f.__name__, MethodType(f, None, cls))


def caller_module_name():
    return inspect.stack()[2][0].f_locals.get('__name__', None)


def parent_module_name(name):
    return name.rsplit('.', 1)[0] if '.' in name else None


def sibling_module_name(name, sibling):
    return (
        lambda pname: sibling if pname is None else pname + '.' + sibling
    )(parent_module_name(name))


def module_path(module):
    return module.__file__.rstrip('/__init__.py').rstrip('/__init__.pyc')


def deep_import(name):

    def _rimport(pkg):
        for _, mname, is_pkg in iter_modules([module_path(pkg)]):
            m = import_module(pkg.__name__ + '.' + mname)
            _rimport(m) if is_pkg else None

    _rimport((
        ignore_exc(import_module, ImportError)(name)
        or sibling_module_name(caller_module_name(), name)
    ))


def ignore_exc(f, *exceptions):

    if isinstance(f, partial):
        def _1(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except exceptions:
                return None

        return _1

    else:
        @wraps(f)
        def _2(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except exceptions:
                return None

        return _2


def cast(t):
    return ignore_exc(t, ValueError)


def concat(delimiter=''):
    return lambda *args: delimiter.join(map(str, args))


def enclose(frame="'"):
    return lambda txt: '{frame}{txt}{frame}'.format(
        frame=frame,
        txt=txt.replace(frame, '\\' + frame)
    )


def dapply(**mapping):
    return lambda d: dict(
        d,
        **dict((k, l(d[k])) for k, l in mapping.items())
    )


def sha256(s):
    return hashlib.sha256(s).hexdigest()


def rand_str(n, readable=False):
    ls = (
        [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'M', 'N', 'P',
            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', '2', '3', '4', '5',
            '6', '7', '8', '9'
        ] if readable
        else list(ascii_lowercase + ascii_uppercase + digits)
    )
    return concat()(*[random.choice(ls) for i in xrange(n)])


def uuid(upper=True):
    return (lambda s: s.upper() if upper else s)(_uuid.uuid4().get_hex())


def unixtime():
    return int(time.time())


def utc_datetime():
    return datetime.utcnow().strftime('%Y/%m/%d %H:%M:%S')


def validate_email(s):
    return re.match(r'[^@]+@[^@]+\.[^@]+', s) is None
