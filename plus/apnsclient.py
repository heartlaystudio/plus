# coding:utf-8

import os

from apns import APNs, Payload


_info = {}


def set_pem(sandbox, pem_path):
    cert_file = 'cert-sandbox.pem' if sandbox else 'cert.pem'
    key_file = 'key-sandbox.pem' if sandbox else 'key.pem'

    _info['apns'] = APNs(
        use_sandbox=sandbox,
        cert_file=os.path.join(pem_path, cert_file),
        key_file=os.path.join(pem_path, key_file)
    )


def send(token, msg):
    try:
        _info['apns'].gateway_server.send_notification(
            token,
            msg.make_payload()
        )
        return True

    except Exception as e:
        print e
        return False


class Message(object):
    def __init__(self, msg, badge=1, sound='default', content_available=False):
        self.msg = msg
        self.badge = badge
        self.sound = sound
        self.content_available

    def make_payload(self):
        return Payload(
            alert=self.msg,
            badge=self.badge,
            sound=self.sound,
            content_available=self.content_available
        )
