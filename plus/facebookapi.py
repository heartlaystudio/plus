# coding:utf-8

import requests

from . import env
from .tools import concat


_URL = 'https://graph.facebook.com/v2.0'
_info = {'appid': None}


def prepare(appid=None):
    _info.update({'appid': appid or env.get('FACEBOOK_APP_ID')})


def get(path, params):
    return (
        lambda r: r.json() if r.status_code == 200 else None
    )(requests.get(_URL + path, params=params))


def validate_token(access_token):
    r = get('/app?fields=id', {'access_token': access_token})
    return True if r and r['id'] == _info['appid'] else False


def get_info(access_token, *fields):
    return get(
        concat(',')('/me?fields=id', *fields),
        {'access_token': access_token}
    ) if validate_token(access_token) else None


def get_friends(self, access_token):
    if self.validate(access_token):

        def _(path):
            r = get(path, {'access_token': access_token})
            if r is not None:
                return r['data'], r['paging']
            else:
                return [], {}

        path = '/me/friends?fields=id'
        while True:
            data, paging = _(path)
            for u in data:
                yield u['id']

            if 'next' in paging:
                path = paging['next'].lstrip(_URL)
            else:
                break
