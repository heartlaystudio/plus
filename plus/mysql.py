# coding:utf-8

from sqlalchemy.exc import IntegrityError, DataError

from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy.engine import ResultProxy

from . import env
from .tools import enclose, cast, concat, add_method


def dictlist(self):
    return [dict(r) for r in self]

add_method(ResultProxy, dictlist)


_sqlalchemy = SQLAlchemy()
_dbinfo = {}


def prepare(**info):
    _dbinfo.update(info or {
        'user': env.get('MYSQL_USER'),
        'password': env.get('MYSQL_PASSWORD'),
        'uri': env.get('MYSQL_URI'),
        'db': env.get('MYSQL_DB'),
    })


def init_app(app):
    uri = 'mysql://{user}:{password}@{uri}/{db}'.format(**_dbinfo)
    app.config['SQLALCHEMY_DATABASE_URI'] = uri

    _sqlalchemy.init_app(app)


def query(q, *args, **kwargs):
    return _sqlalchemy.session.execute(
        ' '.join(q.format(*args, **kwargs).replace('\n', ' ').split())
    )


def scalar(q, *args, **kwargs):
    return query(q, *args, **kwargs).scalar()


def first(q, *args, **kwargs):
    return query(q, *args, **kwargs).first()


def many(q, *args, **kwargs):
    return query(q, *args, **kwargs).dictlist()


def commit():
    _sqlalchemy.session.execute('COMMIT')


def last_id(table):
    return _sqlalchemy.session.execute(
        'SELECT LAST_INSERT_ID() FROM `{}`'.format(table)
    ).scalar()


def sqlval(v):
    return enclose()(str(v))


def sqllist(ls):
    enc = enclose()
    vals = concat(',')(*[enc(str(v)) for v in cast(list)(ls) or []])

    return '({})'.format(str(vals).rstrip(']').lstrip('['))
