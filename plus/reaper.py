# coding:utf-8

from flask import jsonify, abort


_default_msg = {
    400: 'bad access',
    401: 'unauthorized',
    403: 'forbidden',
    404: 'not found',
    405: 'method not allowed',
    409: 'conflict',
    415: 'unsupported media type',
    500: 'internal server error',
    501: 'not implemented',
}


def init_app(app):
    for k, v in _default_msg.items():
        def f(err, msg):
            def _(e):
                return jsonify(message=msg), err
            return _
        app.register_error_handler(k, f(k, v))


def kill(status_code, message=None):
    err_msg = message or _default_msg[status_code]
    response = jsonify(message=err_msg)
    response.status_code = status_code
    abort(response)


def killif(expr, status_code=400, message=None):
    kill(status_code, message) if expr else None


def must(v, lmd=lambda v: v is not None, msg=None):
    return v if lmd(v) else kill(400, msg)
