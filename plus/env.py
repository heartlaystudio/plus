# coding:utf-8

import os


_defaults = {}


def set_defaults(**kwargs):
    _defaults.update(kwargs)


def get(key):
    return os.environ[key] if key in os.environ else _defaults[key]


def debug():
    return get('STAGE') != 'production'
