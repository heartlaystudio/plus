# codign:utf-8

import json

from .tools import concat

DELIMITER_WIDTH = 75


def red_print(s, linebreak=True):
    _color_print(s, linebreak, '\033[91m')


def green_print(s, linebreak=True):
    _color_print(s, linebreak, '\033[92m')


def yellow_print(s, linebreak=True):
    _color_print(s, linebreak, '\033[93m')


def _color_print(s, linebreak, color):
    txt = '{}{}\033[0m'.format(color, s)

    if linebreak:
        print txt
    else:
        print txt,


def line():
    return '-' * DELIMITER_WIDTH


def title(s):
    return concat('\n')(line(), s, line())


def readable(obj):
    return eval("u'''{}'''".format(
        json.dumps(obj, indent=4, sort_keys=True)
    ))
