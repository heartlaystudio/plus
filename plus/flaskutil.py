# coding:utf-8


import re
from importlib import import_module
from operator import itemgetter

from flask import Flask, request

from .tools import caller_module_name, concat


def create_webserver(**kwargs):
    app = Flask(
        caller_module_name(),
        **dict((k, v) for k, v in kwargs.iteritems() if not k.isupper())
    )

    app.config.update(dict(
        {'JSONIFY_PRETTYPRINT_REGULAR': False},
        **dict((k, v) for k, v in kwargs.iteritems() if k.isupper())
    ))

    return app


def create_worker(handler):
    app = Flask(caller_module_name())
    app.debug = True

    @app.route('/', methods=['POST'])
    def run():
        message = request.json
        tag = message['tag']

        f = getattr(handler, tag)
        return '', f(message)

    return app


def create_doc(app, path='/help', table_width=130):
    if not app.debug:
        return

    method_width = 6
    doc_width = 80
    rule_width = table_width - method_width - doc_width - 10

    table_fmt = '| {{:{}s}} | {{:{}s}} | {{:{}s}}'.format(
        method_width, rule_width, doc_width
    )
    row_separator = '|{}'.format(concat('|')(
        '-'*(method_width+2), '-'*(rule_width + 2), '-'*(doc_width + 2)
    ))

    routes = []
    for r in app.url_map.iter_rules():
        if not r.rule.startswith('/static/'):

            rule = re.sub(r'<(.*?):(.*?)>', r':\2', r.rule)

            f = lambda x: x in ['GET', 'POST', 'PUT', 'DELETE']
            method = filter(f, r.methods)[0]

            mname, fname = r.endpoint.rsplit('.', 1)
            doc = getattr(import_module(mname), fname).__doc__

            routes.append((rule, method, doc))

    routes = sorted(routes, key=itemgetter(0, 1))

    help_doc_lines = [row_separator]
    for rule, method, doc in routes:
        doclines = (doc or 'no doc available yet.').split('\n')
        for n, line in enumerate(doclines):
            ls = [method, rule, line] if n == 0 else ['', '', line]
            help_doc_lines.append(table_fmt.format(*ls))

        help_doc_lines.append(row_separator)

    @app.route(path, methods=['GET'])
    def _():
        return concat('\n')(*help_doc_lines)
