# coding:utf-8

import re
from functools import wraps

from flask import Blueprint, request, jsonify

from . import reaper
from .tools import deep_import, caller_module_name, sibling_module_name


_INVALID_ARG_MSG = r'{}\(\)(.*) takes (.*) arguments? \(\d+ given\)'
_routes = {}


def scan(name):
    deep_import(sibling_module_name(caller_module_name(), name))


def bind(pkg, **kwargs):
    bp = Blueprint(pkg, pkg, **kwargs)
    for k, ls in _routes.iteritems():
        if k.startswith(pkg):
            for rule, f, kwargs in ls:
                bp.add_url_rule(rule, view_func=f, **kwargs)
    return bp


def get(rule, **kwargs):
    return _register_route('GET', rule, **kwargs)


def post(rule, **kwargs):
    return _register_route('POST', rule, **kwargs)


def put(rule, **kwargs):
    return _register_route('PUT', rule, **kwargs)


def delete(rule, **kwargs):
    return _register_route('DELETE', rule, **kwargs)


def _register_route(method, rule, **kwargs):
        kwargs['methods'] = [method]

        def _(f):
            @wraps(f)
            def __(*args, **_kwargs):
                if kwargs['methods'][0] == 'GET':
                    _kwargs.update(request.args.items())

                else:
                    _kwargs.update(request.form.items())
                    _kwargs.update(request.files.items())

                try:
                    return f(*args, **_kwargs)

                except TypeError, e:
                    regex = _INVALID_ARG_MSG.format(f.__name__)
                    if re.match(regex, e.message):
                        reaper.kill(400, 'invalid argments')
                    else:
                        raise

            _routes.setdefault(caller_module_name(), []).append((
                rule,
                __,
                kwargs,
            ))

            return __

        return _


def jsonize(f):

    @wraps(f)
    def _(*args, **kwargs):
        return (
            lambda r: jsonify(**r) if r is not None else jsonify()
        )(f(*args, **kwargs))

    return _
