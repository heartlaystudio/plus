# coding;utf-8

import os
import re
import requests

from . import printer


origin = 'http://localhost:5000'
interactive = False

_session = requests.Session()
_routes = []
_results = []


def init_app(app):
    for r in app.url_map.iter_rules():
        if not r.rule.startswith(app.static_url_path):

            for m in r.methods:
                if m not in ['HEAD', 'OPTIONS']:

                    _routes.append((m, r.rule))


def clear():
    os.system('clear')
    os.system('clear')


def test_code_base(instance_name='doctor'):
    return '\n'.join([
        "{}.{}('{}')".format(instance_name, m.lower(), r)
        for m, r in _routes
    ])


def get(path, **params):
    return _request('GET', path, params=params)


def post(path, **params):
    return _request('POST', path, data=params)


def put(path, **params):
    return _request('PUT', path, data=params)


def delete(path, **params):
    return _request('DELETE', path, data=params)


def _request(method, path, params={}, data={}):
    res = _session.request(method, origin + path, params=params, data=data)
    _results.append((method, path, params, res))

    print printer.line()
    _print_status_code(res.status_code)
    print path, 'with {}'.format(_readable_params(dict(params, **data)))
    print printer.line()
    print printer.readable(res.json())

    raw_input() if interactive else None

    return res


def check_coverage():
    tested = []

    for rt_method, rt_rule in _routes:
        for res_method, res_path, _, _ in _results:
            if res_method == rt_method:
                regex = re.sub(r'<(.*?)>', '(.*?)', rt_rule)
                if re.match(regex, res_path):
                    tested.append((rt_method, rt_rule))

    untested = set(_routes) - set(tested)
    untested = ['{} {}'.format(m, r) for m, r in untested]

    if len(untested) > 0:
        printer.yellow_print(printer.title('untested routes exist'))
        print printer.readable(untested)
    else:
        printer.yellow_print(printer.title('all routes are tested'))


def preview():
    printer.yellow_print(printer.title('test result preview'))
    for method, path, params, res in _results:
        _print_status_code(res.status_code)
        print method, path, _readable_params(params)


def _print_status_code(code):
    getattr(
        printer,
        'green_print' if code == 200 else 'red_print'
    )(code, linebreak=False)


def _readable_params(params):
    return ', '.join(['{}={}'.format(k, str(v)) for k, v in params.items()])
