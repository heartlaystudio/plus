# coding:utf-8

from functools import wraps

from flask.ext.login import (
    LoginManager,
    login_required,
    login_user,
    logout_user,
    current_user
)

from . import reaper

_login_manager = LoginManager()


def init_app(app):
    _login_manager.init_app(app)

    @_login_manager.user_loader
    def load_user(user_id):
        return _User.get(user_id)


def login(uid, remember=True):
    logout_user()
    login_user(_User(uid) or reaper.kill(401), remember=remember)


def logout():
    logout_user()


def protect(f):

    @wraps(f)
    def __(*args, **kwargs):

        @login_required
        def _(*_args, **_kwargs):
            return f(*_args, **_kwargs)

        return _(*args, **kwargs)

    return __


def get_uid():
    return current_user.user_id


class _User(object):
    @classmethod
    def get(cls, uid):
        return _User(uid) if uid else None

    def __init__(self, uid):
        self.user_id = uid
        self.active = True
        self.anonymous = False
        self.authenticated = True

    def get_id(self):
        return self.user_id

    def is_authenticated(self):
        return self.authenticated

    def is_active(self):
        return self.active

    def is_anonymous(self):
        return self.anonymous
