# coding:utf-8

import json


class SQSMessageTemplate(object):
    def jsonify(self):
        body = {}
        for attr in dir(self):
            if attr.startswith('msg_'):
                body[attr[4:]] = self.__dict__[attr]
        return json.dumps(body)
