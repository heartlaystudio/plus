# coding:utf-8

from boto.s3.key import Key
from boto.s3.bucket import Bucket
from boto.sqs.queue import Queue
from boto.sqs.message import RawMessage

from plus.tools import add_method
from .sqs import SQSMessageTemplate


def _open(self, name):
    k = Key(self)
    k.key = name
    k.replace = False
    return k


def read(self, name):
    obj = self.get_key(name)
    return obj.get_contents_as_string() if obj is not None else None


def from_file(self, *args, **kwargs):
    self.set_contents_from_file(*args, **kwargs)


def from_string(self, *args, **kwargs):
    self.set_contents_from_string(*args, **kwargs)

add_method(Bucket, _open, 'open')
add_method(Bucket, read)
add_method(Key, from_file)
add_method(Key, from_string)


def add(self, msg):
    if isinstance(msg, SQSMessageTemplate):
        raw_msg = RawMessage()
        raw_msg.set_body(msg.jsonify())
        self.write(raw_msg)

    else:
        raise TypeError('Message must inherit SQSMessageTemplate in plus')


add_method(Queue, add)
