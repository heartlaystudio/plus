# coding:utf-8

import os


def watch_eb_error_log(ipaddress, cert_path):
    cmd = ' '.join([
        'ssh -i {} ec2-user@{}'.format(cert_path, ipaddress),
        #'"sudo tail -f /var/log/httpd/error_log"'
    ])

    os.system(cmd)
