# coding:utf-8


from boto.dynamodb2 import connect_to_region as _dynamodb_connect
from boto.s3 import connect_to_region as _s3_connect
from boto.sns import connect_to_region as _sns_connect
from boto.sqs import connect_to_region as _sqs_connect

from plus import env


def dynamodb(region_name='ap-northeast-1', **kwargs):
    return _dynamodb_connect(region_name, **dict(_env_aws_keys(), **kwargs))


def s3(region_name='ap-northeast-1', **kwargs):
    return _s3_connect(region_name, **dict(_env_aws_keys(), **kwargs))


def sns(region_name='ap-northeast-1', **kwargs):
    return _sns_connect(region_name, **dict(_env_aws_keys(), **kwargs))


def sqs(region_name='ap-northeast-1', **kwargs):
    return _sqs_connect(region_name, **dict(_env_aws_keys(), **kwargs))


def _env_aws_keys():
    return {
        'aws_access_key_id': env.get('AWS_ACCESS_KEY_ID'),
        'aws_secret_access_key': env.get('AWS_SECRET_KEY')
    }
