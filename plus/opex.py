# coding:utf-8

import functools


def fargs(*args, **kwargs):
    return FuncArgs(*args, **kwargs)


def tr(expr):
    return Ternary(expr)


class FuncArgs(object):
    def __init__(self, *args, **kwargs):
        self.args, self.kwargs = args, kwargs

    def __lt__(self, other):  # <
        return functools.partial(other, *(self.args), **(self.kwargs))

    def __gt__(self, other):  # >
        return functools.partial(other, *(self.args), **(self.kwargs))

    def __rlshift__(self, other):  # <<
        return other(*(self.args), **(self.kwargs))

    def __rshift__(self, other):  # >>
        return other(*(self.args), **(self.kwargs))


class Ternary(object):
    def __init__(self, expr):
        self.elems = [expr]

    def __or__(self, other):  # |
        self.elems.append(other)

        if len(self.elems) == 3:
            target = self.elems[1 if self.elems[0] else 2]
            return target() if hasattr(target, '__call__') else target

        else:
            return self
