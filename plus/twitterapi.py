# coding:utf-8

import twitter

from . import env

_info = {}


def prepare(consumer_key=None, consumer_secret=None):
    _info.update({
        'consumer_key': consumer_key or env.get('TWITTER_KEY'),
        'consumer_secret': consumer_secret or env.get('TWITTER_SECRET')
    })


def validate(access_token_key, access_token_secret):
    return twitter.Api(
        consumer_key=_info['consumer_key'],
        consumer_secret=_info['consumer_secret'],
        access_token_key=access_token_key,
        access_token_secret=access_token_secret,
        cache=None
    ).VerifyCredentials()


def get_id(access_token_key, access_token_secret):
    return (
        lambda api: api.id if api else None
    )(validate(access_token_key, access_token_secret))
