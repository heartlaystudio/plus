# coding:utf-8

from setuptools import setup, find_packages

setup(
    name='plus',
    version='0.1.1',
    description='python web application utility',
    author='TomokiNakamaru',
    author_email='tomoki.nakamaru@heartlay-studio.co.jp',
    license='contact me @ tomoki.nakamaru@heartlay-studio.co.jp',
    packages=find_packages(),
    install_requires=[
        'apns==1.1.2',
        'boto==2.30.0',
        'flask==0.10.1',
        'flask-login==0.2.11',
        'flask-sqlalchemy==1.0',
        'mysql-python==1.2.5',
        'python-twitter==1.3.1',
        'requests==2.3.0',
    ]
)
